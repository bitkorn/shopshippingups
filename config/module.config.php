<?php

namespace Bitkorn\ShopShippingUps;

use Bitkorn\ShopShippingUps\Command\ShipConfirmCommand;
use Bitkorn\ShopShippingUps\Controller\Demo\RatingController;
use Bitkorn\ShopShippingUps\Controller\Demo\ShipConfirmController;
use Bitkorn\ShopShippingUps\Factory\Command\ShipConfirmCommandFactory;
use Bitkorn\ShopShippingUps\Factory\Controller\Demo\RatingControllerFactory;
use Bitkorn\ShopShippingUps\Factory\Controller\Demo\ShipConfirmControllerFactory;
use Bitkorn\ShopShippingUps\Factory\Provider\ShippingProviderUpsFactory;
use Bitkorn\ShopShippingUps\Provider\ShippingProviderUps;
use Laminas\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            /**
             * routes for testing comes outside of the module, e.g. in /config/autoload/global.php
             */
//            'bitkorn_shopshippingups_demo_shipconfirm_test' => [
//                'type' => Segment::class,
//                'options' => [
//                    'route' => '/shipping-ups-shipconfirm-test[/:type[/:id]]',
//                    'constraints' => [
//                        'type' => '[a-z]*', // user | basket
//                        'id' => '[0-9]*' // *_address_id
//                    ],
//                    'defaults' => [
//                        'controller' => ShipConfirmController::class,
//                        'action' => 'test',
//                    ],
//                ],
//            ],
//            'bitkorn_shopshippingups_demo_rating_test' => [
//                'type' => Segment::class,
//                'options' => [
//                    'route' => '/shipping-ups-rating-test[/:type[/:id]]',
//                    'constraints' => [
//                        'type' => '[a-z]*', // user | basket
//                        'id' => '[0-9]*' // *_address_id
//                    ],
//                    'defaults' => [
//                        'controller' => RatingController::class,
//                        'action' => 'test',
//                    ],
//                ],
//            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            ShipConfirmController::class => ShipConfirmControllerFactory::class,
            RatingController::class => RatingControllerFactory::class,
        ],
        'invokables' => [],
    ],
    'dependencies' => [
        'factories' => [
            ShipConfirmCommand::class => ShipConfirmCommandFactory::class,
        ],
    ],
    'laminas-cli' => [
        'commands' => [
            'shop-ups:ship-confirm' => ShipConfirmCommand::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            ShippingProviderUps::class => ShippingProviderUpsFactory::class,
        ],
        'invokables' => [],
    ],
    'view_helper_config' => [
        'factories' => [],
        'invokables' => [],
        'aliases' => [],
    ],
    'view_manager' => [
        'template_map' => [],
        'template_path_stack' => [],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];
