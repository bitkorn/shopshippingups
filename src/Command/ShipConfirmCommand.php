<?php

namespace Bitkorn\ShopShippingUps\Command;

use Laminas\Cli\Command\AbstractParamAwareCommand;
use Laminas\Cli\Input\StringParam;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ShipConfirmCommand
 * @package Bitkorn\ShopShippingUps\Command
 * @todo Die Factory muss eine Instanz erstellen
 */
class ShipConfirmCommand extends AbstractParamAwareCommand
{
    /** @var string */
    public static $defaultName = 'shop-ups:ship-confirm';

    protected $test;

    /**
     * @param string $test
     */
    public function setTest(string $test): void
    {
        $this->test = $test;
    }

    protected function configure() : void
    {
        $this->setName(self::$defaultName);
        $this->addParam(
            (new StringParam('name'))
                ->setDescription('Your name')
                ->setShortcut('nm')
        );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * vendor/bin/laminas shop-ups:ship-confirm -name Paula
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getParam('name');
        $output->writeln('Hello ' . $name);
        /**
         * Die Factory wird anscheinend nicht verwendet.
         * Darum hat $this->test den initialen Wert.
         */
        $output->writeln('test: ' . $this->test);
        return 0;
    }

}
