<?php

namespace Bitkorn\ShopShippingUps\Controller\Demo;

use Bitkorn\Shop\Service\Address\ShopAddressService;
use Bitkorn\ShopShippingUps\Provider\ShippingProviderUps;
use Bitkorn\Trinket\Controller\AbstractHtmlController;

class AbstractDemoController extends AbstractHtmlController
{

    /**
     * @var ShopAddressService
     */
    protected $shopAddressService;

    /**
     * @var ShippingProviderUps
     */
    protected $shippingProviderUps;

    /**
     * @param ShopAddressService $shopAddressService
     */
    public function setShopAddressService(ShopAddressService $shopAddressService): void
    {
        $this->shopAddressService = $shopAddressService;
    }

    /**
     * @param ShippingProviderUps $shippingProviderUps
     */
    public function setShippingProviderUps(ShippingProviderUps $shippingProviderUps): void
    {
        $this->shippingProviderUps = $shippingProviderUps;
    }
}
