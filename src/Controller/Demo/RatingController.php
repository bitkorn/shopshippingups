<?php

namespace Bitkorn\ShopShippingUps\Controller\Demo;

use Bitkorn\Trinket\View\Model\JsonModel;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\PhpEnvironment\Response;

class RatingController extends AbstractDemoController
{
	/**
	 * @return JsonModel
	 */
	public function testAction()
	{
        $jsonModel = new JsonModel();
        $accessType = $this->params('type');
        $addressId = $this->params('id');
        if(empty($accessType) || empty($addressId)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $shopAddressEntity = $this->shopAddressService->getAddressById($accessType, $addressId);
        if(!$shopAddressEntity) {
            return $jsonModel;
        }
        $jsonModel->setObj($shopAddressEntity->getStorage());

        $this->shippingProviderUps->setShopAddressEntity($shopAddressEntity);
        $this->shippingProviderUps->setTotalWeight(7200);
        $this->shippingProviderUps->setService();
        $jsonModel->setVariable('ratedShippingCost', $this->shippingProviderUps->getShippingCostsRating());
        $this->logger->info('REQUEST: ' . $this->shippingProviderUps->getRequestXmlRating());
        $this->logger->info('RESULT: ' . $this->shippingProviderUps->getResultXmlRating());
		return $jsonModel;
	}
}
