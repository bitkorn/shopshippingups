<?php

namespace Bitkorn\ShopShippingUps\Controller\Demo;

use Bitkorn\Shop\Entity\Address\ShopAddressEntity;
use Bitkorn\Shop\Service\Address\ShopAddressService;
use Bitkorn\ShopShippingUps\Provider\ShippingProviderUps;
use Bitkorn\Trinket\Controller\AbstractHtmlController;
use Bitkorn\Trinket\View\Model\JsonModel;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\PhpEnvironment\Response;

class ShipConfirmController extends AbstractDemoController
{

    /**
     * @return JsonModel
     */
    public function testAction()
    {
        $jsonModel = new JsonModel();
        $accessType = $this->params('type');
        $addressId = $this->params('id');
        if(empty($accessType) || empty($addressId)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $shopAddressEntity = $this->shopAddressService->getAddressById($accessType, $addressId);
        if(!$shopAddressEntity) {
            return $jsonModel;
        }
        $jsonModel->setObj($shopAddressEntity->getStorage());

        $this->shippingProviderUps->setShopAddressEntity($shopAddressEntity);
        $this->shippingProviderUps->setTotalWeight(7200);
        $this->shippingProviderUps->setService();
        $jsonModel->setVariable('totalShippingCost', $this->shippingProviderUps->getShippingCostsShipConfirm());
        $this->logger->info('REQUEST: ' . $this->shippingProviderUps->getRequestXmlShipConfirm());
        $this->logger->info('RESULT: ' . $this->shippingProviderUps->getResultXmlShipConfirm());
        return $jsonModel;
    }
}
