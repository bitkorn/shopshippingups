<?php


namespace Bitkorn\ShopShippingUps\Factory\Command;


use Bitkorn\ShopShippingUps\Command\ShipConfirmCommand;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShipConfirmCommandFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $cmd = new ShipConfirmCommand();
        $cmd->setTest('lorem ipsum');
        return $cmd;
    }
}
