<?php

namespace Bitkorn\ShopShippingUps\Factory\Controller\Demo;

use Bitkorn\Shop\Service\Address\ShopAddressService;
use Bitkorn\ShopShippingUps\Controller\Demo\ShipConfirmController;
use Bitkorn\ShopShippingUps\Provider\ShippingProviderUps;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShipConfirmControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new ShipConfirmController();
        $controller->setLogger($container->get('logger'));
        $controller->setShopAddressService($container->get(ShopAddressService::class));
        $controller->setShippingProviderUps($container->get(ShippingProviderUps::class));
        return $controller;
    }
}
