<?php


namespace Bitkorn\ShopShippingUps\Factory\Provider;



use Bitkorn\ShippingUps\Service\Kit\RatingUpsServiceKit;
use Bitkorn\ShippingUps\Service\Kit\ShippingUpsServiceKit;
use Bitkorn\ShippingUps\Service\Kit\TrackingUpsServiceKit;
use Bitkorn\ShopShippingUps\Provider\ShippingProviderUps;
use Bitkorn\Trinket\Table\IsoCountryTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShippingProviderUpsFactory implements FactoryInterface
{

    /**
     * @inheritDoc
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $provider = new ShippingProviderUps();
        $provider->setRatingUpsService($container->get(RatingUpsServiceKit::class));
        $provider->setShippingUpsService($container->get(ShippingUpsServiceKit::class));
        $provider->setTrackingUpsService($container->get(TrackingUpsServiceKit::class));
        $provider->setIsoCountryTable($container->get(IsoCountryTable::class));
        return $provider;
    }
}
