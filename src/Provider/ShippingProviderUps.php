<?php

namespace Bitkorn\ShopShippingUps\Provider;

use Bitkorn\ShippingUps\Service\Kit\RatingUpsServiceKit;
use Bitkorn\ShippingUps\Service\Kit\ShippingUpsServiceKit;
use Bitkorn\ShippingUps\Service\Kit\TrackingUpsServiceKit;
use Bitkorn\Shop\Service\Shipping\ShippingProvider\AbstractShippingProvider;
use Bitkorn\Trinket\Table\IsoCountryTable;

class ShippingProviderUps extends AbstractShippingProvider
{
    const UNIQUE_DESCRIPTOR = 'ups';

    /**
     * @var RatingUpsServiceKit
     */
    protected $ratingUpsService;

    /**
     * @var ShippingUpsServiceKit
     */
    protected $shippingUpsService;

    /**
     * @var TrackingUpsServiceKit
     */
    protected $trackingUpsService;

    /**
     * @var IsoCountryTable
     */
    protected $isoCountryTable;

    /**
     * @var string
     */
    protected $requestXmlRating;

    /**
     * @var string
     */
    protected $resultXmlRating;

    /**
     * @var string
     */
    protected $requestXmlShipConfirm;

    /**
     * @var string
     */
    protected $resultXmlShipConfirm;

    /**
     * @param RatingUpsServiceKit $ratingUpsService
     */
    public function setRatingUpsService(RatingUpsServiceKit $ratingUpsService): void
    {
        $this->ratingUpsService = $ratingUpsService;
    }

    /**
     * @param ShippingUpsServiceKit $shippingUpsService
     */
    public function setShippingUpsService(ShippingUpsServiceKit $shippingUpsService): void
    {
        $this->shippingUpsService = $shippingUpsService;
    }

    /**
     * @param TrackingUpsServiceKit $trackingUpsService
     */
    public function setTrackingUpsService(TrackingUpsServiceKit $trackingUpsService): void
    {
        $this->trackingUpsService = $trackingUpsService;
    }

    /**
     * @param IsoCountryTable $isoCountryTable
     */
    public function setIsoCountryTable(IsoCountryTable $isoCountryTable): void
    {
        $this->isoCountryTable = $isoCountryTable;
    }

    public function setService(string $code = '11', string $desc = 'UPS Standard'): void
    {
        $this->shippingUpsService->setShipmentService($code, $desc);
    }

    public function getUniqueDescriptor(): string
    {
        return self::UNIQUE_DESCRIPTOR;
    }

    public function getBrand(): string
    {
        return '<img src="/img/module/shipping-ups/UPS_logo.svg" alt="UPS Logo" style="height: 60px">';
    }

    /**
     * @return string
     */
    public function getBrandText(): string
    {
        return 'UPS';
    }

    /**
     * @return string
     */
    public function getRequestXmlRating(): string
    {
        return $this->requestXmlRating;
    }

    /**
     * @return string
     */
    public function getResultXmlRating(): string
    {
        return $this->resultXmlRating;
    }

    /**
     * @return string
     */
    public function getRequestXmlShipConfirm(): string
    {
        return $this->requestXmlShipConfirm;
    }

    /**
     * @return string
     */
    public function getResultXmlShipConfirm(): string
    {
        return $this->resultXmlShipConfirm;
    }

    /**
     * @return float
     */
    public function getShippingCostsRating(): float
    {
        if(empty($this->resultXmlRating)) {
            if (
                !isset($this->shopAddressEntity)
                || empty($this->shopAddressEntity->getStorage())
                || empty($country = $this->isoCountryTable->getIsoCountryById($this->shopAddressEntity->getAddressCountryId()))
            ) {
                return 0;
            }
            $this->ratingUpsService->setShipmentShipFromDefault();
            $this->ratingUpsService->setShipmentShipTo(
                $this->shopAddressEntity->getAddressName1(),
                $this->shopAddressEntity->getAddressTel(),
                $this->shopAddressEntity->getAddressEmail(),
                $this->shopAddressEntity->getAddressStreetWithNumber(),
                $this->shopAddressEntity->getAddressCity(),
                $this->shopAddressEntity->getAddressZip(),
                $country['country_iso']
            );
            $this->ratingUpsService->setShipmentService();
            $this->ratingUpsService->addShipmentPackage($this->totalWeight);
            $this->requestXmlRating = $this->ratingUpsService->getRatingServiceSelectionRequestXML();
            $this->resultXmlRating = $this->ratingUpsService->doRatingServiceSelectionRequest();
        }
        $elem = simplexml_load_string($this->resultXmlRating);
        if($elem->Response->ResponseStatusCode == 1) {
            $this->shippingCostsRating = floatval($elem->RatedShipment->TotalCharges->MonetaryValue);
        }
        return $this->shippingCostsRating;
    }

    /**
     * @return float
     */
    public function getShippingCostsShipConfirm(): float
    {
        if(empty($this->resultXmlShipConfirm)) {
            if (
                !isset($this->shopAddressEntity)
                || empty($this->shopAddressEntity->getStorage())
                || empty($country = $this->isoCountryTable->getIsoCountryById($this->shopAddressEntity->getAddressCountryId()))
            ) {
                return 0;
            }
            $this->shippingUpsService->setShipmentShipFromDefault();
            $this->shippingUpsService->setShipmentShipTo(
                $this->shopAddressEntity->getAddressName1(),
                $this->shopAddressEntity->getAddressTel(),
                $this->shopAddressEntity->getAddressEmail(),
                $this->shopAddressEntity->getAddressStreetWithNumber(),
                $this->shopAddressEntity->getAddressCity(),
                $this->shopAddressEntity->getAddressZip(),
                $country['country_iso']
            );
            $this->shippingUpsService->setShipmentService();
            $this->shippingUpsService->addShipmentPackage($this->totalWeight);
            $this->requestXmlShipConfirm = $this->shippingUpsService->getShipmentConfirmRequestXML();
            $this->resultXmlShipConfirm = $this->shippingUpsService->doShipmentConfirmRequest();
        }
        $elem = simplexml_load_string($this->resultXmlShipConfirm);
        if($elem->Response->ResponseStatusCode == 1) {
            $this->shippingCostsShipConfirm = floatval($elem->ShipmentCharges->TotalCharges->MonetaryValue);
        }
        return $this->shippingCostsShipConfirm;
    }

}
